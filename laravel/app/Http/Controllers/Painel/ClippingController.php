<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Helpers\Tools;

class ClippingController extends Controller
{
    public function index()
    {
        $registros = Clipping::ordenados()->get();

        return view('painel.clipping.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.clipping.create');
    }

    public function store(ClippingRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Clipping::upload_imagem();
            if (isset($input['video_codigo']) && isset($input['video_tipo'])) $input['video_capa'] = Tools::videoThumb($request->get('video_tipo'), $request->get('video_codigo'));

            Clipping::create($input);
            return redirect()->route('painel.clipping.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Clipping $registro)
    {
        return view('painel.clipping.edit', compact('registro'));
    }

    public function update(ClippingRequest $request, Clipping $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Clipping::upload_imagem();
            if ($input['video_codigo'] !== $registro->video_codigo || $input['video_tipo'] !== $registro->video_tipo) {
                $input['video_capa'] = Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            $registro->update($input);
            return redirect()->route('painel.clipping.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Clipping $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.clipping.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function deleteImage($id)
    {
        try {

            $midia = Clipping::find($id);
            $midia->imagem = null;
            $midia->save();

            return back()->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

}
