var config   = require('../config'),
    gulp     = require('gulp'),
    imagemin = require('gulp-imagemin'),
    plumber  = require('gulp-plumber');

gulp.task('images', function() {
    return gulp.src(config.development.img + '**/*')
        .pipe(plumber())
        .pipe(imagemin({ progressive: true, interlaced: true }))
        .pipe(gulp.dest(config.build.img))
});
