@include('painel.common.flash')

<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            {!! Form::label('texto', 'Texto') !!}
            {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            <img src="{{ url('assets/img/perfil/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
