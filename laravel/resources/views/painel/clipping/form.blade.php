@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar' && $registro->imagem)
    <img src="{{ url('assets/img/clipping/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    <a href="{{ route('painel.clipping.deleteImage', $registro->id) }}" class="btn-delete btn-delete-link label label-danger" style="display:inline-block;margin-bottom:10px">
        <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
        REMOVER
    </a>
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_tipo', 'Vídeo Tipo') !!}
            {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_codigo', 'Vídeo Código') !!}
            {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clipping.index') }}" class="btn btn-default btn-voltar">Voltar</a>
